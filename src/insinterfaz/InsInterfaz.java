/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package insinterfaz;

import javax.swing.JOptionPane;

public class InsInterfaz {

    public static void main(String[] args) {
        int opcion = 0, elemento = 0;
        Cola colita = new Cola();
        Cola a = new Cola();
        int Nestudiantes;
        Nestudiantes = 10 + (int) (Math.random() * 15);
        do {
            try {
                opcion = Integer.parseInt(JOptionPane.showInputDialog(null, "1.- Insertar elementos a la cola\n"
                        + "2.- Quitar a los que duren mas de 50 segundos en la cola\n"
                        + "3.- Desplegar cola\n"
                        + "4.-Salir",
                        "Menu de opciones de la cola", JOptionPane.QUESTION_MESSAGE));

                switch (opcion) {
                    case 1:

                        for (int i = 0; i <= Nestudiantes; i++) {
                            colita.insertar();
                        }
                        break;
                    case 2:
                        for (int i = 0; i <= Nestudiantes; i++) {
                            colita.eliminar();
                        }
                        break;
                    case 3:
                        colita.desplegarCola();
                        break;
                    case 4:
                        JOptionPane.showMessageDialog(null, "Aplicacion finalizada ", "Fin",
                                JOptionPane.INFORMATION_MESSAGE);
                        break;
                    default:
                        JOptionPane.showMessageDialog(null, "Opcion incorrecta ", "Cuidado",
                                JOptionPane.INFORMATION_MESSAGE);
                }
            } catch (NumberFormatException n) {
                JOptionPane.showMessageDialog(null, "Error " + n.getMessage());
            }
        } while (opcion != 4);
    }
}
