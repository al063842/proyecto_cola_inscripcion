package insinterfaz;

/**
 *
 * @author ameri
 */
public class Cola {

    Nodo inicio, fin, actual;

    public void insertar() {
        Nodo nuevo = new Nodo();
        int tiempo = 10 + (int) (Math.random() * 60);
        nuevo.dato = tiempo;
        if (inicio == null) {
            inicio = nuevo;
            inicio.siguiente = null;
            fin = nuevo;
        } else {
            fin.siguiente = nuevo;
            nuevo.siguiente = null;
            fin = nuevo;
        }

    }

    public void desplegarCola() {
        int Ttiempo;
        Nodo actual = new Nodo();
        actual = inicio;
        int Estudiante = 1;
        if (inicio != null) {
            while (actual != null) {
                System.out.println("Estudiante " + Estudiante++ + "   tiempo que espero para ser atendido: " + "[" + actual.dato + " segundos]");
                actual = actual.siguiente;
            }
        }
    }

    public void eliminar() {

        Nodo actual = new Nodo();
        Nodo anterior = new Nodo();
        anterior = null;
        actual = inicio;

        if (inicio != null) {
            while (actual != null) {
                if (actual.dato <= 50) {
                    System.out.println("El alumno ha sido atendido");
                } else if (actual.dato > 50) {
                    if (actual == inicio) {
                        inicio = inicio.siguiente;
                    } else if (actual == fin) {
                        anterior.siguiente = null;
                        fin = anterior;
                    } else {
                        anterior.siguiente = actual.siguiente;
                    }
                    System.out.println("El alumno tardo mas de 50 segundos y decidio salirse de la cola");
                }
                anterior = actual;
                actual = actual.siguiente;
            }
        }
    }

}
